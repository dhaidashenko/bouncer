package bouncer_test

import (
	"github.com/pkg/errors"
	bouncer "gitlab.com/dhaidashenko/bouncer/pkg/v1"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestUsage(t *testing.T) {
	b := bouncer.New(bouncer.Opts{})

	t.Run("no header", func(t *testing.T) {
		r, _ := http.NewRequest("", "", nil)
		_, err := b.Check(r, bouncer.Identity{})
		assert.Equal(t, bouncer.ErrInvalidToken, errors.Cause(err))
	})

	t.Run("invalid header", func(t *testing.T) {
		r, _ := http.NewRequest("", "", nil)
		r.Header.Set("Authorization", "Bearerfoobar")
		_, err := b.Check(r, bouncer.Identity{})
		assert.Equal(t, bouncer.ErrInvalidToken, errors.Cause(err))
	})

	t.Run("missing token", func(t *testing.T) {
		r, _ := http.NewRequest("", "", nil)
		r.Header.Set("Authorization", "Bearer ")
		_, err := b.Check(r, bouncer.Identity{})
		assert.Equal(t, bouncer.ErrInvalidToken, errors.Cause(err))
	})

	t.Run("empty claims", func(t *testing.T) {
		claims := bouncer.Claims{}
		r := bouncer.AuthedRequest(t, claims)
		assert.Panics(t, func() {
			_,_ = b.Check(r)
		})
	})

	t.Run("failing rule", func(t *testing.T) {
		claims := bouncer.Claims{
			IdentityID: "123",
		}
		r := bouncer.AuthedRequest(t, claims)
		_, err := b.Check(r, bouncer.Identity{ID: new(string)})
		assert.Equal(t, bouncer.ErrForbidden, errors.Cause(err))
	})

	t.Run("passing rule", func(t *testing.T) {
		id := "123"
		claims := bouncer.Claims{
			IdentityID: id,
		}
		r := bouncer.AuthedRequest(t, claims)
		got, err := b.Check(r, bouncer.Identity{
			ID: &id,
		})
		require.NoError(t, err)
		require.Equal(t, &claims, got)
	})

	t.Run("multi pass", func(t *testing.T) {
		id := "123"
		claims := bouncer.Claims{
			IdentityID: id,
		}
		r := bouncer.AuthedRequest(t, claims)
		got, err := b.Check(r, bouncer.Identity{
			ID: new(string),
		}, bouncer.Identity{ID: &id})
		require.NoError(t, err)
		require.Equal(t, &claims, got)
	})
}

func TestUsageSkipChecks(t *testing.T) {
	b := bouncer.New(bouncer.Opts{
		SkipChecks: true,
	})

	t.Run("no header", func(t *testing.T) {
		r, _ := http.NewRequest("", "", nil)
		_, err := b.Check(r, bouncer.Identity{})
		assert.NoError(t, err)
	})

	t.Run("empty claims", func(t *testing.T) {
		claims := bouncer.Claims{}
		r := bouncer.AuthedRequest(t, claims)
		got, err := b.Check(r, bouncer.Identity{})
		require.NoError(t, err)
		require.Equal(t, &claims, got)
	})

	t.Run("failing rule", func(t *testing.T) {
		claims := bouncer.Claims{}
		r := bouncer.AuthedRequest(t, claims)
		got, err := b.Check(r, bouncer.Identity{ID: new(string)})
		require.NoError(t, err)
		require.Equal(t, &claims, got)
	})

	t.Run("passing rule", func(t *testing.T) {
		id := "123"
		claims := bouncer.Claims{
			IdentityID: id,
		}
		r := bouncer.AuthedRequest(t, claims)
		got, err := b.Check(r, bouncer.Identity{ID: &id})
		require.NoError(t, err)
		require.Equal(t, &claims, got)
	})

	t.Run("multi pass", func(t *testing.T) {
		id := "123"
		claims := bouncer.Claims{
			IdentityID: id,
		}
		r := bouncer.AuthedRequest(t, claims)
		got, err := b.Check(r, bouncer.Identity{ID: new(string)}, bouncer.Identity{ID: &id})
		require.NoError(t, err)
		require.Equal(t, &claims, got)
	})
}

func TestUsageRuleFunc(t *testing.T) {
	rule := bouncer.RuleFunc(func(claims bouncer.Claims) bool {
		// assert claims here
		return true
	})

	assert.Implements(t, (*bouncer.Rule)(nil), rule)
}
