package bouncer

import (
	"gitlab.com/distributed_lab/logan/v3/errors"
	"net/http"
)

var (
	//ErrForbidden - requestor does not have sufficient permission to perform request -
	// non of the rules have not returned non nil response
	ErrForbidden = &forbidden{"forbidden"}
	//ErrInvalidToken - token is malformed, expired or not present
	ErrInvalidToken = &notallowed{"invalid token"}
)

type bouncer struct {
	opts Opts
}

type Opts struct {
	// SkipChecks make any request with valid or missing token pass
	SkipChecks  bool
}

func New(opts Opts) Bouncer {
	return &bouncer{opts}
}

//Check - checks against specified rules. Panics if no rules specified (to get claims without rules use ParseClaims)
// Returns: ErrInvalidToken -
// Returns ErrForbidden
func (c bouncer) Check(r *http.Request, rules ...Rule) (*Claims, error) {
	if len(rules) == 0 {
		panic(errors.New("at least one rule must be specified"))
	}
	claims, err := ParseClaims(r)
	if err != nil {
		if errors.Cause(err) == ErrInvalidToken && c.opts.SkipChecks {
			return nil, nil
		}

		return nil, errors.Wrap(err, "failed to parse jwt")
	}

	if c.opts.SkipChecks {
		return claims, nil
	}

	if claims == nil {
		return nil, ErrInvalidToken
	}

	for _, rule := range rules {
		if rule.IsAuthorized(*claims) {
			return claims, nil
		}
	}

	return nil, ErrForbidden
}
