package bouncer

import (
	jwtgo "github.com/dgrijalva/jwt-go"
	"github.com/stretchr/testify/require"
	"net/http"
	"testing"
)

func AuthedRequest(t *testing.T, claims Claims) *http.Request {
	t.Helper()
	r, _ := http.NewRequest("", "", nil)
	token := NewWithClaims(jwtgo.SigningMethodHS256, claims)
	signed, err := token.SigningString()
	require.NoError(t, err)
	r.Header.Set(claimsHeader, signed + ".stab_signature")
	return r
}
