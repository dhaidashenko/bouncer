package bouncer

// implements interface defined in ape
type forbidden struct {
	msg string
}

func (e *forbidden) Error() string {
	return e.msg
}

func (*forbidden) Forbidden() bool {
	return true
}

// implements interface defined in ape
type notallowed struct {
	msg string
}

func (e *notallowed) Error() string {
	return e.msg
}

func (*notallowed) NotAllowed() bool {
	return true
}
