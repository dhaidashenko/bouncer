package bouncer

import (
	"github.com/google/uuid"
	"net/http"
)

// The RuleFunc type is an adapter to allow the use of
// ordinary functions as Rule
type RuleFunc func(Claims) bool

func (r RuleFunc) IsAuthorized(claims Claims) bool {
	return r(claims)
}

type Rule interface {
	IsAuthorized(Claims) bool
}

type Bouncer interface {
	Check(r *http.Request, rules ...Rule) (*Claims, error)
}

// Identity allows to check constraints not bound to particular identity type.
type Identity struct {
	ID *string
	UUID *uuid.UUID
}

func (a Identity) IsAuthorized(claims Claims) bool {
	if a.ID != nil {
		if *a.ID != claims.IdentityID {
			return false
		}
	}

	if a.UUID != nil {
		if a.UUID.String() != claims.IdentityID {
			return false
		}
	}

	return true
}

