package bouncer

import (
	"crypto/ecdsa"
	jwtgo "github.com/dgrijalva/jwt-go"
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/comfig"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"time"
)

type Gatewayer interface {
	Gateway() Gateway
}

func NewGatewayer(getter kv.Getter, storage SessionProvider, log *logan.Entry) Gatewayer {
	return &gatewayer{
		getter:  getter,
		storage: storage,
		log:     log,
	}
}

type gatewayer struct {
	getter  kv.Getter
	once    comfig.Once
	storage SessionProvider
	log     *logan.Entry
}

//gatewayConfig - defines config for gateway - authentication method verifier
type gatewayConfig struct {
	PublicKey    *ecdsa.PublicKey `fig:"-"`
	RawPublicKey string           `fig:"public_key,required"`
	Origins      []string         `fig:"origins"`
	Headers      []string         `fig:"headers"`
	Methods      []string         `fig:"methods"`
}

func (c *gatewayer) Gateway() Gateway {
	return c.once.Do(func() interface{} {
		var cfg gatewayConfig
		err := figure.Out(&cfg).From(kv.MustGetStringMap(c.getter, "gateway")).Please()
		if err != nil {
			panic(errors.Wrap(err, "failed to figure out gateway config"))
		}

		cfg.PublicKey, err = jwtgo.ParseECPublicKeyFromPEM([]byte(cfg.RawPublicKey))
		if err != nil {
			panic(errors.Wrap(err, "invalid ecdsa public key for gateway"))
		}

		return Gateway(&gateway{
			cfg:     cfg,
			storage: c.storage,
			now: func() time.Time {
				return time.Now().UTC()
			},
			log: c.log,
		})

	}).(Gateway)
}
