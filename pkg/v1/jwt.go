package bouncer

import (
	"github.com/dgrijalva/jwt-go"
)

type (
	StandardClaims = jwt.StandardClaims
)

var (
	NewWithClaims      = jwt.NewWithClaims
	SigningMethodES256 = jwt.SigningMethodES256
)

type Claims struct {
	IdentityID         string `json:"identity_id"`
	jwt.StandardClaims `log:"-"`
}
