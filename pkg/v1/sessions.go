package bouncer

import (
	"context"
	"github.com/google/uuid"
	"time"
)

//AuthenticationType - defines authentication type of
type AuthenticationType string

const (
	//AuthenticationTypeJWT - session ID + jwt token
	AuthenticationTypeJWT AuthenticationType = "jwt"
	//AuthenticationTypeBearer - session ID
	AuthenticationTypeBearer AuthenticationType = "bearer"
)

//SessionProvider - allows to access stored sessions
type SessionProvider interface {
	Get(context.Context, uuid.UUID) (*Session, error)
}

// Session - represents auth primitive that could be controlled by identity
type Session struct {
	ID        uuid.UUID
	Identity  uuid.UUID
	Removed   bool
	ExpiresAt time.Time
	AuthType  AuthenticationType
}
