package bouncer

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"encoding/pem"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"io/ioutil"
	"os"
	"path"
)

//GenerateKeys - generates keys used to sign JWT
func GenerateKeys(log *logan.Entry, directoryPath string) error {
	private, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		return errors.Wrap(err, "failed to generate ecdsa key")
	}

	privateBytes, err := x509.MarshalECPrivateKey(private)
	if err != nil {
		return errors.Wrap(err, "failed to x509 marhal private key")
	}

	publicBytes, err := x509.MarshalPKIXPublicKey(private.Public())
	if err != nil {
		return errors.Wrap(err, "failed to x509 marhal public key")
	}

	var pemPrivateBlock = &pem.Block{
		Type:  "EC PRIVATE KEY",
		Bytes: privateBytes,
	}

	var pemPublicBlock = &pem.Block{
		Type:  "EC PUBLIC KEY",
		Bytes: publicBytes,
	}

	privateBuffer := new(bytes.Buffer)
	publicBuffer := new(bytes.Buffer)

	err = pem.Encode(privateBuffer, pemPrivateBlock)
	if err != nil {
		return errors.Wrap(err, "failed to pem encode private block")
	}
	err = pem.Encode(publicBuffer, pemPublicBlock)
	if err != nil {
		return errors.Wrap(err, "failed to pem encode public block")
	}

	pkFile := path.Join(directoryPath, "ecdsa.key")
	err = ioutil.WriteFile(pkFile, privateBuffer.Bytes(), os.ModePerm)
	if err != nil {
		return errors.Wrap(err, "failed write private key file")
	}

	pubFile := path.Join(directoryPath,  "ecdsa.key.pub")
	err = ioutil.WriteFile(pubFile, publicBuffer.Bytes(), os.ModePerm)
	if err != nil {
		return errors.Wrap(err, "failed write public key file")
	}

	log.Infof("private key: %s; public key: %s", pkFile, pubFile)

	return nil
}
