package bouncer_test

import (
	"github.com/stretchr/testify/assert"
	bouncer "gitlab.com/dhaidashenko/bouncer/pkg/v1"
	"testing"
)

func TestParseWithClaims(t *testing.T) {
	t.Run("parse from cookies", func(t *testing.T) {
		claims := bouncer.Claims{
			IdentityID: "account-id",
		}

		r := bouncer.AuthedRequest(t, claims)

		got, err := bouncer.ParseClaims(r)
		assert.NoError(t, err, "failed to parse claims")

		assert.Equal(t, claims, *got)
	})
}
