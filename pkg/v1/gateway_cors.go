package bouncer

import (
	"fmt"
	"github.com/rs/cors"
	"gitlab.com/distributed_lab/logan/v3"
	"net/http"
)

type corsLogger struct {
	*logan.Entry
}

func (c corsLogger) Printf(str string, args ...interface{}) {
	c.Debug(fmt.Sprintf(str, args...))
}

func (g *gateway) GetCors() func(http.Handler) http.Handler{
	c := cors.New(cors.Options{
		AllowedOrigins:   g.cfg.Origins,
		AllowedHeaders:   g.cfg.Headers,
		AllowedMethods:   g.cfg.Methods,
		AllowCredentials: true,
		Debug:            true,
	})

	c.Log = corsLogger{Entry: g.log.WithField("service", "cors")}
	return c.Handler
}
