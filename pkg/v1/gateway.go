package bouncer

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"net/http"
	"strings"
	"time"
)

type Gateway interface {
	// GetAuthHeadersSanitizer returns middleware that sanitizes auth headers based on config provided
	GetAuthHeadersSanitizer() func(http.Handler) http.Handler
	GetCors() func(http.Handler) http.Handler
}

//GetAuthHeadersSanitizer - returns middleware that sanitizes auth headers based on config provided
func (g *gateway) GetAuthHeadersSanitizer() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			g.sanitize(r)
			next.ServeHTTP(w, r)
		})
	}
}

type gateway struct {
	cfg     gatewayConfig
	storage SessionProvider
	now     func() time.Time
	log     *logan.Entry
}

func parseSessionID(r *http.Request) (uuid.UUID, error) {
	rawSessionID := r.Header.Get(AuthorizationHeader)
	// handle `Bearer <ID>` case
	splits := strings.Split(rawSessionID, " ")
	id := splits[len(splits)-1]
	return uuid.Parse(id)
}

func (g *gateway) sanitize(r *http.Request) {
	r.Header.Set(gatewayHeader, g.now().String())
	r.Header.Del(claimsHeader)

	sessionID, err := parseSessionID(r)
	if err != nil {
		return
	}

	session, err := g.storage.Get(r.Context(), sessionID)
	if err != nil {
		panic(errors.Wrap(err, "failed to get session id by id"))
	}

	if session == nil {
		return
	}

	if session.Removed || session.ExpiresAt.Before(g.now()) {
		return
	}

	switch session.AuthType {
	case AuthenticationTypeJWT:
		g.handleJWT(r, *session)
	case AuthenticationTypeBearer:
		g.handleBearer(r, *session)
	default:
		panic(errors.From(errors.New("unexpected auth type"), logan.F{
			"auth_type": session.AuthType,
		}))
	}

}

func (g *gateway) handleBearer(r *http.Request, session Session) {
	token := NewWithClaims(SigningMethodES256, Claims{
		IdentityID: session.Identity.String(),
		StandardClaims: jwt.StandardClaims{
			Id:        session.ID.String(),
			ExpiresAt: session.ExpiresAt.Unix(),
		},
	})

	tokenStr, err := token.SigningString()
	if err != nil {
		panic(errors.Wrap(err, "failed to get signed string"))
	}

	r.Header.Set(claimsHeader, tokenStr+".bearer_token_stub_signature")
}

func (g *gateway) handleJWT(r *http.Request, session Session) {
	token, claims, err := g.getJWT(r)
	if err != nil {
		return
	}

	if claims.Id != session.ID.String() {
		return
	}

	r.Header.Set(claimsHeader, token)
}

func (g *gateway) getJWT(r *http.Request) (string, *Claims, error) {
	cookie, err := r.Cookie(CookieName)
	if err != nil {
		return "", nil, errors.Wrap(err, "failed to get cookie")
	}

	if cookie == nil {
		return "", nil, errors.New("cookie is nil")
	}

	var claims Claims
	_, err = jwt.ParseWithClaims(cookie.Value, &claims, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodECDSA); !ok {
			return nil, errors.From(errors.New("unexpected signing method"), logan.F{
				"alg": token.Header["alg"],
			})
		}
		return g.cfg.PublicKey, nil
	})
	if err != nil {
		return "", nil, errors.Wrap(err, "failed to validate claims")
	}

	now := g.now()
	if time.Unix(claims.ExpiresAt, 0).Before(now) {
		return "", nil, errors.New("jwt expired")
	}

	return cookie.Value, &claims, nil
}

// TODO: cover with tests
