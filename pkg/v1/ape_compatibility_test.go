package bouncer_test

import (
	"github.com/stretchr/testify/assert"
	bouncer "gitlab.com/dhaidashenko/bouncer/pkg/v1"
	"gitlab.com/distributed_lab/ape/problems"
	"net/http"
	"testing"
)

func TestApeCompatibility(t *testing.T) {
	b := bouncer.New(bouncer.Opts{
		SkipChecks:  false,
	})
	t.Run("no token", func(t *testing.T) {
		r, _ := http.NewRequest("", "", nil)
		_, err := b.Check(r, bouncer.Identity{})
		assert.NotNil(t, err)
		p := problems.NotAllowed(err)
		assert.Equal(t, p.Title, http.StatusText(http.StatusUnauthorized))
	})
	t.Run("no enough permission", func(t *testing.T) {
		r := bouncer.AuthedRequest(t, bouncer.Claims{
			IdentityID:     "123",
		})

		_, err := b.Check(r, bouncer.Identity{
			ID: new(string),
		})
		assert.NotNil(t, err)
		p := problems.NotAllowed(err)
		assert.Equal(t, p.Title, http.StatusText(http.StatusForbidden))
	})
}
