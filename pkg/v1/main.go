package bouncer

import (
	"net/http"

	jwtgo "github.com/dgrijalva/jwt-go"
)

const (
	gatewayHeader       = "X-Gateway"
	claimsHeader        = "X-Verified-JWT"
	CookieName          = "auth_348c070" // random suffix is needed to eliminate probability of names collision
	AuthorizationHeader = "Authorization"
)

func GatewayPassed(r *http.Request) bool {
	return r.Header.Get(gatewayHeader) != ""
}

var parser = jwtgo.Parser{}

//ParseClaims - parses claims without signature verification and token validation.
// Returns ErrInvalidToken if token is malformed or not present
func ParseClaims(r *http.Request) (*Claims, error) {
	rawClaims := r.Header.Get(claimsHeader)
	if rawClaims == "" {
		return nil, ErrInvalidToken
	}

	var claims Claims
	_, _, err := parser.ParseUnverified(rawClaims, &claims)
	if err != nil {
		return nil, ErrInvalidToken
	}

	return &claims, nil
}
